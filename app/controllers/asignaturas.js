var moongose = require("mongoose"),
    Asignaturas = require("../models/asignatura");

exports.load = async () => {
    var res = await Asignaturas.find({})
    return res;
}
exports.save = (req) => {
    var newAsignaturas = new Asignaturas(req);
    newAsignaturas.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}