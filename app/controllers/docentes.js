var moongose = require("mongoose"),
    Docentes = require("../models/docente");

exports.load = async () => {
    var res = await Docentes.find({})
    return res;
}
exports.save = (req) => {
    var newDocentes = new Docentes(req);
    newDocentes.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}