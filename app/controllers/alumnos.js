var moongose = require("mongoose"),
    Alumnos = require("../models/alumnos");

exports.load = async () => {
    var res = await Alumnos.find({})
    return res;
}
exports.save = (req) => {
    var newAlumnos = new Alumnos(req);
    newAlumnos.save((err, res) => {
        if (err) console.log(err);
        console.log("Insertado en la BD");
        return res;
    });
}