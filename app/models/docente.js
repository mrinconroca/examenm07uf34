var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var docenteSchema = new Schema({
    Nombre: { type: String },
    Apellido: { type: String }
});

module.exports = mongoose.model("Asignatura", docenteSchema);