var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var asignaturaSchema = new Schema({
    Nombre: { type: String },
    Numhoras: { type: String },
    Docente: { type: Schema.Types.ObjectId },
    Alumnos: { type: [Schema.Types.ObjectId], default: undefined }
});

module.exports = mongoose.model("Asignatura", asignaturaSchema);