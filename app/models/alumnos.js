var mongoose = require("mongoose"),
    Schema = mongoose.Schema

var chatSchema = new Schema({
    Nombre: { type: String },
    Apellido: { type: String }
});

module.exports = mongoose.model("Chat", chatSchema);