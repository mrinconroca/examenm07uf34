var express = require("express");
const { router } = require("../../app");
router = express.Router();
var alumnCntr = require(__dirname + "/../controllers/alumnos");
var asignCntr = require(__dirname + "/../controllers/asignaturas");

router.route('/chat').get(async (req, res, next) => {
    var result = await alumnCntr.load();
    console.log("Chat loaded")
    console.log(result)
    res.render("chat", result[0]);
});

router.route('/chat/:sala').get(async (req, res, next) => {
    var result = await alumnCntr.load();
    console.log("Chat loaded sala:" + req.params.sala)
    console.log(result)
    if (req.params.sala == "all") {
        res.render("chat", { "sala": "" });
    } else {
        res.render("chat", { "sala": req.params.sala });
    }
});

router.route('/asignatura').get(async (req, res, next) => {
    var result = await asignCntr.load();
    console.log("Asignatura loaded")
    console.log(result)
    res.render("newAsignatura");
});


router.route('/asignatura/new').get((req, res, next) => {
    var newAsignatura = {
        Nombre: 'Mates',
        Numhoras: '4h',
        Docente: [
            {
                Nombre: "Juan",
                Apellido: "Sanz"
            }
        ],
        Alumnos: [
            {
                Nombre: "Sara",
                Apellido: "Campos"
            },
            {
                Nombre: "Daniel",
                Apellido: "Marcos"
            }
        ]
    };
    asignCntr.save(newAsignatura);
    res.send("Guardado");
})

router.route('/asignatura/save').get((req, res, next) => {
    var newAsignatura = {
        Nombre: req.body.asignatura,
        Numhoras: req.body.numhoras,
        Docente: [
            {
                Nombre: req.body.profesor
            }
        ],
        Alumnos: [
            {
                Nombre: req.body.alumnos
            }
        ]
    };
    asignCntr.save(newAsignatura);
    res.send("Guardado");
})

module.exports = router;