var express = require("express");
router = express.Router();
var alumnosCtrl = require(__dirname + "/../controllers/alumnos");
var asignaturasCtrl = require(__dirname + "/../controllers/asignaturas");
var docentesCtrl = require(__dirname + "/../controllers/docentes");


router.route("/").get(async (req, res, next) => {
    var result = await chatCntr.load();
    res.render("chat", result[0]);
});

router.route('/new').get((req, res, next) => {
    var newChat = {
        user1: '/user/1',
        user2: 'user/2',
        msg: [
            {
                user: 1,
                text: "hola que tal"
            },
            {
                user: 2,
                text: "todo bien gracias"
            }
        ]
    };
    chatCntr.save(newChat);
    res.send("Guardado");
})

module.exports = router;